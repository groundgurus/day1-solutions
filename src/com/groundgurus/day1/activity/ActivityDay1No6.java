package com.groundgurus.day1.activity;

import java.util.Scanner;

/**
 *
 * @author pgutierrez
 */
public class ActivityDay1No6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter width: ");
        int width = scanner.nextInt();
        
        System.out.print("Enter height: ");
        int height = scanner.nextInt();
        
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
        
    }
}
